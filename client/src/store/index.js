import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
  },
  getters: {
    // get方法
    getUsername(state) {
      return localStorage.getItem('username');
    },
    getEmail(state) {
      return localStorage.getItem('email');
    },
    getRole(state) {
      return localStorage.getItem('role');
    },
    getAvatar(state) {
      return localStorage.getItem('avatar');
    }
  },
  mutations: {
    // 使用多个action commit触发
    setToken(state, token) {
      localStorage.setItem('token', token);
    },
    clearToken(state) {
      localStorage.removeItem('token');
    },
    setEmail(state, email) {
      localStorage.setItem('email', email);
    },
    setUsername(state, username) {
      localStorage.setItem('username', username);
    },
    setRole(state, role) {
      localStorage.setItem('role', role);
    },    
    setAvatar(state, avatar) {
      if (avatar == null) 
        avatar = 'https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif';
      localStorage.setItem('avatar', avatar);
    }
  },
  actions: {
    // 使用多个mutations dispatch触发 异步
  },
  modules: {
  },
});
