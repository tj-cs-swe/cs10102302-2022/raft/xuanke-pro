import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';

import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import 'font-awesome/scss/font-awesome.scss';
import './assets/icons'; // icon

import VueParticles from 'vue-particles';
Vue.use(VueParticles);
Vue.use(ElementUI);

Vue.config.productionTip = false;

import axios from 'axios';
Vue.prototype.$axios = axios;
// 默认请求的基础url(如果axios请求的地址不带域名,自动添加baseURL(默认请求域名，/当前域名)
axios.defaults.baseURL = 'http://localhost:3000/';

// 设置post请求头的content-Type值
axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';

// 请求超时5000毫秒
axios.defaults.timeout = 5000;

// cookie跨域
axios.defaults.withCredentials = true;

// 请求拦截
axios.interceptors.request.use(config => {
  // 在发送请求之前做某件事
  // 如果存在token,则每个http header都加上token
  if (localStorage.getItem('token')) {
    config.headers.Authorization = `Bearer ${localStorage.getItem('token')}`;
  }
  return config;
}, error => {
  // 对请求错误做些什么
  return Promise.reject(error);
});

// 响应拦截
axios.interceptors.response.use(response => {
  // 对响应数据做些事
  return response;
}, error => {
  // 获取错误状态码 token失效
  const { status } = error.response;
  if (status === 401) {
    ElementUI.Message.error('身份验证失效，请重新登录');
    store.commit('clearToken');
    router.push('/');
  }
  return Promise.reject(error);
});

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount('#app');
