import Vue from 'vue';
import VueRouter from 'vue-router';
import CourseInfo from'../views/CourseInfo.vue';
import LoginView from '../views/LoginView.vue';
import SearchView from'../views/SearchView.vue';
import AccountView from '../views/AccountView.vue';

const originalPush = VueRouter.prototype.push;
VueRouter.prototype.push = function push(location) {
  return originalPush.call(this, location).catch(err => err);
};

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: '登录',
    component: LoginView,
  },
  {
    path: '/courseInfo/:courseId',
    name: 'CourseInfo',
    component: CourseInfo,
  },
  {
    path: '/search',
    name: 'search',
    component: SearchView,
  },
  {
    path: '/account',
    name: 'account',
    component: AccountView,
  },
  {
    path: '*',
    redirect: '/',
  }
];

const router = new VueRouter({
  mode: 'history', //去掉#号
  routes,
});

const vm = new Vue();

router.beforeEach((to, from, next) => {
  if (to.path === '/') {
    next();
  } 
  else if (to.path === null) {
    next('/');
  }
  else {
    if (localStorage.getItem('token')) {
      next();
    } 
    else {
      vm.$message.error('身份验证失败，请先登录');
      next('/');
    }
  }
});

export default router;