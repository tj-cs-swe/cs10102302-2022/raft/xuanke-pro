const express = require('express');
const router = express.Router();
const usersController = require('../controllers/users');
const coursesController = require('../controllers/courses');
const coursesinfoController = require('../controllers/courseinfos');
const commentsController = require('../controllers/comments');
const questionsController = require('../controllers/questions');
const accountController = require('../controllers/account');
const { sendEmail, checkCode, forgetEmail} = require('../controllers/email');
const repliesController = require('../controllers/replies');
const uploadController = require('../controllers/upload');

const multer = require('multer');
var upload = multer({ dest:'images/' });

// 注册相关
router.get('/auth/check-emailcode', checkCode);
router.post('/auth/send-emailcode', sendEmail);

// 账号相关
router.get('/login/user', accountController.login);
router.post('/auth/user', accountController.add);
router.post('/auth/resest-password', accountController.resetPassword);
router.post('/auth/change-password', accountController.changePassword);

// 忘记密码
router.post('/auth/forget-emailcode', forgetEmail);

// 课程相关
router.post('/courses/add-courses', coursesController.add_courses);
router.get('/courses/get-courses', coursesController.get_courses);
// 课程信息
router.get('/courseinfo/get-courseinfo', coursesinfoController.get_courseinfo);
router.post('/courseinfo/update-courseinfo', coursesinfoController.update_courseinfo);

// 课程评论
router.get('/comment/get-comment', commentsController.get_comment);
router.post('/comment/like-comment', commentsController.like_comment);
router.post('/comment/add-comment', commentsController.add_comment);
router.post('/comment/delete-comment', commentsController.delete_comment);


// 用户信息相关
router.get('/user/info', usersController.get_userinfo);
router.post('/user/post-info', usersController.add_userinfo);
router.get('/courses/get-timetable', usersController.get_timetable2);
router.get('/user/collections', usersController.get_collection);
router.post('/user/collections', usersController.add_collection);
router.delete('/user/collections', usersController.delete_collection);
router.post('/user/courses', usersController.add_timetable);
router.get('/user/courses', usersController.get_timetable2);
router.post('/user/avatar', usersController.add_avatar);
router.delete('/user/courses', usersController.delete_timetable);

// 问题相关
router.get('/forum/get-question', questionsController.get_question);
router.get('/forum/search-content', questionsController.search_content);
router.post('/forum/add-question', questionsController.add_question);
router.post('/forum/delete-question', questionsController.delete_question);
router.post('/forum/like-question', questionsController.like_question);
router.post('/forum/stick-question', questionsController.stick_question);

// 回答相关
router.get('/forum/get-reply', repliesController.get_reply);
router.post('/forum/add-reply', repliesController.add_reply);
router.post('/forum/delete-reply', repliesController.delete_reply);
router.post('/forum/like-reply', repliesController.like_reply);

// 上传文件
router.post('/upload/upload-image', upload.single('images'), uploadController.upload_image);

module.exports = router;