const { query } = require('express');
const Model = require('../model');
const { Comments } = Model;

const coursesinfoController = {
  // 获取评论
  get_comment(req, res) {
    var mquery = Comments.find({ course_id: req.query['course_id'] });
    if (req.query['idx_type'] == 0) {
      mquery.sort({ like_count: -1 });
    } else if (req.query['idx_type'] == 1) {
      mquery.sort({ time: -1 });
    }
    mquery.exec((err, comments) => {
      if (err) {
        res.status(400).json({
          code: 400,
          msg: '查询失败',
          err: err,
        });
      } else {
        res.json({
          code: 200,
          msg: '查询成功',
          list_size: comments.length,
          comment_list: comments,
        });
      }
    });
  },
  // 添加评论
  add_comment(req, res) {
    var comment = new Comments({
      course_id: req.body['course_id'],
      user_id: req.body['user_id'],
      score: req.body['score'],
      content: req.body['content'],
      time: Date.parse(new Date()),
      like_count: 0,
      liked: []
    });
    comment.save((err) => {
      if (err) {
        res.status(400).json({
          code: 400,
          msg: '添加失败',
          err: err,
        });
      } else {
        res.json({
          code: 200,
          msg: '添加成功',
        });
      }
    });
  },
  // 点赞评论
  like_comment(req, res) {
    var mquery = Comments.find({
      _id: req.body['comment_id']
    });
    if (Boolean(parseInt(req.body['like_type'])) == true) {
      mquery.update({
        $push: {
          liked: req.body['user_id']
        },
        $inc: {
          like_count: 1
        },
      });
    } else {
      mquery.update({
        $pull: {
          liked: req.body['user_id']
        },
        $inc: {
          like_count: -1
        },
      });
    }
    mquery.exec((err, comment) => {
      if (err) {
        res.status(400).json({
          code: 400,
          msg: '修改失败',
          err: err,
        });
      } else {
        res.json({
          code: 200,
          like_state: Boolean(parseInt(req.body['like_type'])),
          msg: '修改成功',
        });
      }
    });
  },
  // 删除评论
  delete_comment(req, res) {
    Comments.findOneAndDelete({ _id: req.body['comment_id'] }, (err, comment) => {
      if (err) {
        res.status(400).json({
          code: 400,
          msg: '删除失败',
          err: err,
        });
      } else {
        res.json({
          code: 200,
          msg: '删除成功',
        });
      }
    }); 
  },
};

module.exports = coursesinfoController;