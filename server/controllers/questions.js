const { query } = require('express');
const Model = require('../model');
const { Replies, Questions } = Model;

const fs = require('fs');

const questionsController = {
  // 获取问题
  async get_question(req, res) {
    var mquery = Questions.find({
      course_id: req.query['course_id'],
    });
    if (req.query['idx_type'] == 0) {
      mquery.sort({ sticked: -1, reply_stamp: -1 });
    } else if (req.query['idx_type'] == 1) {
      mquery.sort({ sticked: -1, timestamp: -1 });
    } else if (req.query['idx_type'] == 2) {
      mquery.sort({ sticked: -1, timestamp: 1 });
    }
    var mquery_count = mquery.clone();
    const counter = await mquery_count.count();
    //需处理请求失败情况
    mquery.skip(req.query['idx_up']);
    mquery.limit(req.query['idx_low'] - req.query['idx_up']);
    mquery.exec((err, question) => {
      if (err) {
        console.log(err);
        res.status(400).json({
          code: 400,
          msg: '查询失败',
          err: err,
        });
      } else {
        question_list = [];
        question.forEach(element => {
          let x = false;
          element['liked'].forEach(like => {
            if (req.query['user_id'] == like) {
              x = true;
            }
          });
          let qs = {
            question_id: element['_id'],
            question_head: element['head'],
            thumbnail: element['thumbnail'],
            user_id: element['user_id'],
            reply_timestamp: element['reply_stamp'],
            reply_count: element['reply_cnt'],
            like_count: element['liked'].length,
            like_state: x,
            stick_state: element['sticked'],
          };
          question_list.push(qs);
        });
        res.json({
          code: 200,
          msg: '查询成功',
          question_count: counter,
          list_len: question.length,
          question_list: question_list,
        });
      }
    });
  },
  // 添加问题
  add_question(req, res) {
    var question = new Questions({
      course_id: req.body['course_id'],
      head: req.body['question_head'],
      content: req.body['question_content'],
      thumbnail: req.body['thumbnail'],
      user_id: req.body['user_id'],
      image_list: req.body['image_list'],
      timestamp: Date.parse(new Date()),
      reply_stamp: Date.parse(new Date()),
      reply_cnt: 0,
      liked: [],
      sticked: false,
    });
    question.save((err) => {
      if (err) {
        res.status(400).json({
          code: 400,
          msg: '添加失败',
          err: err,
        });
      } else {
        res.json({
          code: 200,
          msg: '添加成功',
          question_id: question['_id']
        });
      }
    });
  },
  // 删除问题
  delete_question(req, res) {
    Questions.findOneAndDelete({ _id: req.body['question_id'] }, (err, question) => {
      if (err) {
        res.status(400).json({
          code: 400,
          msg: '删除失败',
          err: err,
        });
      } else {
        question.image_list.forEach(element => {
          fs.unlink('/images/'+element);
        });
        res.json({
          code: 200,
          msg: '删除成功',
          question: question
        });
      }
    });
  },
  // 添加点赞
  like_question(req, res) {
    var mquery = Questions.find({
      _id: req.body['question_id']
    });
    if (Boolean(parseInt(req.body['like_type'])) == true) {
      mquery.update({
        $push: {
          liked: req.body['user_id']
        }
      });
    } else {
      mquery.update({
        $pull: {
          liked: req.body['user_id']
        }
      });
    }
    mquery.exec((err, question) => {
      if (err) {
        res.status(400).json({
          code: 400,
          msg: '修改失败',
          err: err,
        });
      } else {
        res.json({
          code: 200,
          like_state: Boolean(parseInt(req.body['like_type'])),
          msg: '修改成功'
        });
      }
    });
  },
  // 置顶问题
  stick_question(req, res) {
    var mquery = Questions.find({
      _id: req.body['question_id']
    });
    if (Boolean(parseInt(req.body['stick_type'])) == true) {
      mquery.update({
        $set: {
          sticked: 1
        },
      });
    } else {
      mquery.update({
        $set: {
          sticked: 0
        },
      });
    }
    mquery.exec((err, question) => {
      if (err) {
        res.status(400).json({
          code: 400,
          msg: '置顶失败',
          err: err,
        });
      } else {
        res.json({
          code: 200,
          stick_state: Boolean(parseInt(req.body['stick_type'])),
          msg: '置顶成功'
        });
      }
    });
  },
  // 查询内容
  async search_content(req, res) {
    sc = req.query['search_content'];
    const question = await Questions.find({
      course_id: req.query['course_id'],
      $or: [
        {
          content: {
            $regex: sc,
            $options: 'i'
          }
        },
        {
          head: {
            $regex: sc,
            $options: 'i'
          }
        }
      ]
    });
    const question_all = await Questions.find({
      course_id: req.query['course_id'],
    });
    rplq = Replies.find({
      content: {
        $regex: sc,
        $options: 'i',
      }
    });
    rplq.where('_id').in(question_all);
    const answer = await rplq.exec();
    let ql = [];
    answer.forEach((item) => {
      ql.push(item['question_id']);
    });
    question.forEach((item) => {
      ql.push(item['_id']);
    });
    let mq = Questions.find({});
    mq.where('_id').in(ql);
    if (req.query['idx_type'] == 0) {
      mq.sort({ reply_stamp: -1 });
    } else if (req.query['idx_type'] == 1) {
      mq.sort({ timestamp: -1 });
    } else if (req.query['idx_type'] == 2) {
      mq.sort({ timestamp: 1 });
    }
    mq.skip(req.query['idx_up']);
    mq.limit(req.query['idx_low'] - req.query['idx_up']);
    mq.exec((err, ans) => {
      if (err) {
        res.status(400).json({
          code: 400,
          msg: '查询失败',
          err: err,
        });
      } else {
        let question_list = [];
        ans.forEach(element => {
          let x = false;
          element['liked'].forEach(like => {
            if (req.query['user_id'] == like) {
              x = true;
            }
          });
          let qs = {
            question_id: element['_id'],
            question_head: element['head'],
            thumbnail: element['thumbnail'],
            user_id: element['user_id'],
            reply_timestamp: element['reply_stamp'],
            reply_count: element['reply_cnt'],
            like_count: element['liked'].length,
            like_state: x,
            stick_state: element['sticked'],
          };
          question_list.push(qs);
        });
        res.json({
          code: 200,
          msg: '查询成功',
          question_count: ql.length,
          list_len: question.length,
          question_list: question_list,
        });
      }
    });
  }  
};

module.exports = questionsController;
